#ifndef __BLOCK_H__
#define __BLOCK_H__

#include "config.h"

class Block
{
	/*
	 *	界面中方块的相关属性:
	 *
	 *	type		-> 该方块的类型，其值可以为空、数字或是地雷
	 *	number		-> 该方块上数字的值
	 *	isCovered	-> 该方块是否被覆盖
	 *	isMarked	-> 该方块是否被标记
	 *	isError		-> 该方块是否被错误标记
	 *	isTouched	-> 该方块处的地雷是否被触碰到
	 */

	private:
		int type;
		int number;
		bool isCovered;
		bool isMarked;
		bool isError;
		bool isTouched;

	//	以下为相关属性的获取操作

	public:
		int getNumber();
		bool getIsNull();
		bool getIsMine();
		bool getIsNumber();
		bool getIsCovered();
		bool getIsMarked();
		bool getIsError();
		bool getIsTouched();

	//	以下为该方块在游戏中的相关操作

	public:
		void initData();
		void addMine();
		void addNumber();
		void loseOperation();
		void leftClick();
		void rightClick(int&);
};

#endif
