#include <QApplication>

#include "minesweeper.h"
#include "login.h"

int main(int argc, char *argv[])
{
	QApplication app(argc, argv);

	//	创建登录界面和游戏界面窗口对象

	LoginWindow login;
	MainGame game;

	//	连接两个窗口的信号和槽

	QObject::connect(&login, SIGNAL(loginSignal(User)), &game, SLOT(loginSlot(User)));
	QObject::connect(&game, SIGNAL(logoutSignal()), &login, SLOT(logoutSlot()));

	login.show();

	//	初始化游戏数据

	game.loadImage();
	game.loadAudio();
	game.initColor();
	game.setInterval();
	game.connectTimer();
	game.connectAction();
	game.startTimer();

	//	游戏开始运行

	return app.exec();
}
