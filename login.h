#ifndef __LOGIN_H__
#define __LOGIN_H__

#include <QMainWindow>
#include <QMessageBox>

#include "config.h"
#include "user.h"

QT_BEGIN_NAMESPACE

namespace Ui
{
	class LoginWindow;
}

QT_END_NAMESPACE

//	登录界面的窗口类

class LoginWindow : public QMainWindow
{
	Q_OBJECT

	private:
		Ui::LoginWindow *ui;
		User userTemp;

	//	此窗口通过信号槽与游戏主窗口建立连接，传递用户数据

	signals:
		void loginSignal(User);

	private slots:
		void logoutSlot();
		void login();
		void registry();

	public:
		LoginWindow(QWidget *parent = nullptr);
		~LoginWindow();
};

#endif
