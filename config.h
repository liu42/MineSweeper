#ifndef __CONFIG_H__
#define __CONFIG_H__

#undef NULL

//  窗口的基本参数

#define WINDOW_TITLE "Mine Sweeper"
#define GAME_FPS 10
#define NUMBER_COUNT 8
#define CLOCK_INTERVAL 1000
#define AI_THREAD_INTERVAL 10

//  初级难度的游戏数据

#define TABLE_ROWS_EASY 10
#define TABLE_COLS_EASY 10
#define MINE_INIT_COUNT_EASY 10

//  中级难度的游戏数据

#define TABLE_ROWS_NORMAL 15
#define TABLE_COLS_NORMAL 15
#define MINE_INIT_COUNT_NORMAL 30

//  高级难度的游戏数据

#define TABLE_ROWS_HIGH 30
#define TABLE_COLS_HIGH 20
#define MINE_INIT_COUNT_HIGH 100

//  游戏各种数据的最大值

#define TABLE_ROWS_MAX 40
#define TABLE_COLS_MAX 25

//  游戏界面绘制的相关参数

#define BLOCK_WIDTH 32
#define MARGIN_X 12
#define MARGIN_Y 78
#define BORDER 6
#define BLOCK_BORDER 1

//  游戏界面的颜色数据

#define COLOR_WHITE 0xFFFFFFFF
#define COLOR_GRAY 0xFF606060
#define COLOR_BLACK 0xFF353535

//  游戏界面上方提示信息的相关参数

#define INFO_POSITION 53
#define INFO_MAX_LEN 30
#define INFO_WIDTH 120

//  游戏运行的四种状态

#define PLAYING 0
#define PAUSE 1
#define OVER 2
#define WIN 3
#define EXIT 4

//	游戏中的两种操作类型

#define LEFT_CLICK 1
#define RIGHT_CLICK 2

//  游戏界面中方块的三种类型

#define NULL 0
#define MINE 1
#define NUMBER 2

//  游戏的四种难度等级

#define EASY_LEVEL 0
#define NORMAL_LEVEL 1
#define HIGH_LEVEL 2
#define CUSTOM_LEVEL 3

//  排行榜数组相关参数

#define LEVEL_COUNT 3
#define RECORD_COUNT 3

//  排行榜标签相关参数

#define RECORD_LABEL_WIDTH 160
#define RECORD_LABEL_HEIGHT 20
#define RECORD_LABEL_LEFT 50
#define RECORD_LABEL_TOP 90
#define RECORD_LABEL_LEFT_STEP 200
#define RECORD_LABEL_TOP_STEP 45

//	用户信息标签相关参数

#define USER_INFO_LABEL_WIDTH 100
#define USER_INFO_LABEL_HEIGHT 20
#define USER_INFO_LABEL_LEFT 160
#define USER_INFO_LABEL_TOP 130
#define USER_INFO_LABEL_STEP 30

//	AI扫雷模式的相关参数

#define AI_THREAD_INTERVAL 10
#define LINKBLOCK_SIZE_LIMIT 32

#endif
