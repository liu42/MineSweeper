#ifndef __MINESWEEPER_H__
#define __MINESWEEPER_H__

#include <QMainWindow>
#include <QDesktopWidget>
#include <QMediaPlayer>
#include <QMouseEvent>
#include <QTimer>
#include <QPainter>
#include <QString>
#include <QThread>

#include "config.h"
#include "dialog.h"
#include "block.h"
#include "user.h"
#include "ai.h"

QT_BEGIN_NAMESPACE

namespace Ui
{
	class MainGame;
}

QT_END_NAMESPACE

//  游戏中的图像资源模块

struct Image
{
	QPixmap block;
	QPixmap cover;
	QPixmap error;
	QPixmap mine;
	QPixmap mineError;
	QPixmap flag;
	QPixmap number[NUMBER_COUNT];
};

//  游戏中的定时器模块

struct Timer
{
	QTimer interval;
	QTimer clock;
};

//  游戏中的颜色数据模块

struct Color
{
	QColor white;
	QColor gray;
	QColor black;
};

//  游戏中的音频资源模块

struct Audio
{
	QMediaPlayer click;
	QMediaPlayer lose;
	QMediaPlayer win;
};

class MainGame;

//	AI模式主程序线程类

class AIMainThread : public QThread
{
	private:
		MainGame* game;

	protected:
		virtual void run();

	public:
		void link(MainGame*);
		void stop();
};

//  游戏的主类 MainGame

class MainGame : public QMainWindow
{
	friend class AIMainThread;

	Q_OBJECT

	//  游戏中的各个模块

	private:
		Ui::MainGame* ui;
		QPoint mouse;
		Image image;
		Timer timer;
		Color color;
		Audio audio;

	//	AI扫雷模式的相关模块

	private:
		MineSweeperAI AIMode;
		AIMainThread AIThread;

	//  游戏中所有的对话框

	private:
		AboutDialog about;
		CustomDialog custom;
		RecordDialog record;
		UserInfoDialog userInfo;

	/*
	 *  游戏的所有数据信息:
	 *
	 *  block	 -> 游戏界面每个位置的对象数组
	 *  mineList -> 界面中所有地雷的位置信息
	 *	user	 -> 当前登录的用户
	 */

	private:
		Block block[TABLE_ROWS_MAX][TABLE_COLS_MAX];
		QVector <Point> mineList;
		User user;

	//  游戏界面的相关参数，变量名中 screen 指的是整个窗口，table 指的是游戏界面

	private:
		int screenWidth;
		int screenHeight;
		int tableWidth;
		int tableHeight;
		int tableRows;
		int tableCols;
		int mineInitCount;

	/*
	 *  游戏运行的相关数据:
	 *
	 *  status          -> 游戏当前的运行状态
	 *  flagCount       -> 剩余的旗子数量
	 *	nullCount		-> 当前空白位置的数量
	 *  timeSec         -> 游戏流逝的时间 (以秒为单位)
	 *  level           -> 游戏当前的难度等级
	 *  isCracked       -> 游戏当前是否处于作弊模式
	 *  ifHaveCracked	-> 是否开启过作弊模式
	 *	isInAIMode		-> 游戏当前是否处于AI模式
	 *	ifHaveAIMode	-> 是否开启过AI模式
	 */

	private:
		int status;
		int flagCount;
		int nullCount;
		int timeSec;
		int level;
		bool isOpenFinish;
		bool isCracked;
		bool ifHaveCracked;
		bool isInAIMode;
		bool ifHaveAIMode;

	signals:
		void logoutSignal();

	private slots:
		void loginSlot(User);
		void logout();

	public:
		MainGame(QWidget *parent = nullptr);
		~MainGame();

	//  游戏调整难度的相关函数

	public:
		void setEasyLevel();
		void setNormalLevel();
		void setHighLevel();
		void setCustomLevel();
		void resizeWindow();

	//  游戏初始化的相关函数

	public:
		void loadImage();
		void loadAudio();
		void mainInterval();
		void clockCallback();
		void initGame();
		void initColor();
		void setInterval();
		void connectTimer();
		void connectAction();
		void startTimer();

	//  游戏运行的相关函数

	public:
		void addMine();
		void addNumber();
		void setPauseStatus();
		void gameoverWin();
		void gameoverLose();
		void update();
		void mousePressEvent(QMouseEvent*);
		void keyPressEvent(QKeyEvent*);
		void keyReleaseEvent(QKeyEvent*);
		void displayBackground(QPainter&);
		void displayBlock(QPainter&);
		void displayInfo(QPainter&);
		void paintEvent(QPaintEvent*);
};

#endif
