#ifndef __DIALOG_H__
#define __DIALOG_H__

#include <QDialog>
#include <QString>
#include <QLabel>

#include "config.h"
#include "user.h"

QT_BEGIN_NAMESPACE

namespace Ui
{
	class AboutDialog;
	class CustomDialog;
	class RecordDialog;
	class UserInfoDialog;
}

QT_END_NAMESPACE

//	排行榜中的数据结构体

struct Record
{
	QString name;
	int time;
};

//  关于页面对话框类

class AboutDialog : public QDialog
{
	Q_OBJECT

	private:
		Ui::AboutDialog *ui;

	public:
		AboutDialog(QWidget *parent = nullptr);
		~AboutDialog();
};

//  自定义游戏难度页面对话框类

class CustomDialog : public QDialog
{
	Q_OBJECT

	//  成员变量 ifNeedSet 记录是否需要重设自定义游戏难度

	private:
		Ui::CustomDialog *ui;
		bool ifNeedSet;

	public:
		CustomDialog(QWidget *parent = nullptr);
		~CustomDialog();

	public:
		void displayDialog();
		void closeDialog();
		bool getIfNeedSet();
		int getInputRows();
		int getInputCols();
		int getInputMineCount();
};

/*
 *  排行榜页面对话框类:
 *
 *  采用二维数组构建排行榜标签
 *  数组的第一维表示不同的难度等级
 *  数组的第二维表示不同的数据记录
 */

class RecordDialog : public QDialog
{
	Q_OBJECT

	//  页面控件相关对象

	private:
		Ui::RecordDialog *ui;
		QLabel *recordLabel[LEVEL_COUNT][RECORD_COUNT];

	private:
		QVector <User> userList;
		QVector <Record> recordList;

	public:
		RecordDialog(QWidget *parent = nullptr);
		~RecordDialog();

	public:
		static bool compare(Record&, Record&);
		void initLabel();
		void deleteLabel();
		void displayDialog();
};

//	用户信息对话框类

class UserInfoDialog : public QDialog
{
	Q_OBJECT

	private:
		Ui::UserInfoDialog *ui;
		QLabel *recordLabel[LEVEL_COUNT];
		User *user;

	public:
		UserInfoDialog(QWidget *parent = nullptr);
		~UserInfoDialog();

	public:
		void initLabel();
		void deleteLabel();
		void setUser(User*);
		void displayDialog();
};

#endif
