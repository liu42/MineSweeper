#include "ui_about.h"
#include "ui_custom.h"
#include "ui_record.h"
#include "ui_user.h"

#include "dialog.h"

//  关于页面对话框类成员函数

AboutDialog::AboutDialog(QWidget *parent) : QDialog(parent), ui(new Ui::AboutDialog)
{
	ui->setupUi(this);
}

AboutDialog::~AboutDialog()
{
	delete ui;
}

//  自定义游戏难度页面对话框类成员函数

CustomDialog::CustomDialog(QWidget *parent) : QDialog(parent), ui(new Ui::CustomDialog)
{
	ui->setupUi(this);
	connect(ui->doneButton, &QPushButton::clicked, this, &CustomDialog::closeDialog);
}

CustomDialog::~CustomDialog()
{
	delete ui;
}

void CustomDialog::displayDialog()
{
	ifNeedSet = false;
	this->exec();
}

void CustomDialog::closeDialog()
{
	ifNeedSet = true;
	this->close();
}

bool CustomDialog::getIfNeedSet()
{
	return ifNeedSet;
}

int CustomDialog::getInputRows()
{
	return ui->widthSpinBox->value();
}

int CustomDialog::getInputCols()
{
	return ui->heightSpinBox->value();
}

int CustomDialog::getInputMineCount()
{
	//  如果地雷数量超过窗口的限制，则将地雷数量设置为窗口的宽乘高，以此来限制用户输入的地雷数量

	if (ui->mineSpinBox->value() >= ui->widthSpinBox->value() * ui->heightSpinBox->value() - 1)
	{
		return ui->widthSpinBox->value() * ui->heightSpinBox->value() - 1;
	}
	else { return ui->mineSpinBox->value(); }
}

//  排行榜页面对话框类成员函数

RecordDialog::RecordDialog(QWidget *parent) : QDialog(parent), ui(new Ui::RecordDialog)
{
	ui->setupUi(this);
	initLabel();
}

RecordDialog::~RecordDialog()
{
	delete ui;
	deleteLabel();
}

bool RecordDialog::compare(Record& record1, Record& record2)
{
	//  按照用时从小到大排序的比较函数

	return record1.time < record2.time;
}

void RecordDialog::initLabel()
{
	//  初始化排行榜中各个数据标签的参数

	for (int level = EASY_LEVEL; level <= HIGH_LEVEL; level++)
	{
		for (int i = 0; i < RECORD_COUNT; i++)
		{
			static QRect labelRect;

			labelRect.setX(RECORD_LABEL_LEFT + RECORD_LABEL_LEFT_STEP * level);
			labelRect.setY(RECORD_LABEL_TOP + RECORD_LABEL_TOP_STEP * i);
			labelRect.setWidth(RECORD_LABEL_WIDTH);
			labelRect.setHeight(RECORD_LABEL_HEIGHT);

			recordLabel[level][i] = new QLabel(this);
			recordLabel[level][i]->setGeometry(labelRect);
		}
	}
}

void RecordDialog::deleteLabel()
{
	//	释放 recordLabel 所占用的内存空间

	for (int level = EASY_LEVEL; level < HIGH_LEVEL; level++)
	{
		for (int i = 0; i < RECORD_COUNT; i++)
		{
			delete recordLabel[level][i];
		}
	}
}

void RecordDialog::displayDialog()
{
	//  在窗口打开前获取排行榜数据并格式化输出到窗口中

	static char text[INFO_MAX_LEN];

	userList = User::getUserList();

	for (int level = EASY_LEVEL; level <= HIGH_LEVEL; level++)
	{
		for (int i = 0; i < userList.size(); i++)
		{
			recordList.append({ userList[i].getName(), userList[i].getRecord(level) });
		}
		std::sort(recordList.begin(), recordList.end(), compare);

		for (int i = 0; i < RECORD_COUNT; i++)
		{
			if (i < recordList.size())
			{
				sprintf(text, "%s (%ds)", recordList[i].name.toStdString().data(), recordList[i].time);
				recordLabel[level][i]->setText((recordList[i].time != INT_MAX) ? text : "~");
			}
			else { recordLabel[level][i]->setText("~"); }
		}
		recordList.clear();
	}
	this->exec();
}

UserInfoDialog::UserInfoDialog(QWidget *parent) : QDialog(parent), ui(new Ui::UserInfoDialog)
{
	ui->setupUi(this);
	initLabel();
}

UserInfoDialog::~UserInfoDialog()
{
	delete ui;
	deleteLabel();
}

void UserInfoDialog::initLabel()
{
	//	初始化用户数据标签

	for (int level = EASY_LEVEL; level <= HIGH_LEVEL; level++)
	{
		static QRect labelRect;

		labelRect.setX(USER_INFO_LABEL_LEFT);
		labelRect.setY(USER_INFO_LABEL_TOP + USER_INFO_LABEL_STEP * level);
		labelRect.setWidth(USER_INFO_LABEL_WIDTH);
		labelRect.setHeight(USER_INFO_LABEL_HEIGHT);

		recordLabel[level] = new QLabel(this);
		recordLabel[level]->setGeometry(labelRect);
	}
}

void UserInfoDialog::deleteLabel()
{
	//	释放用户数据标签的内存

	for (int level = EASY_LEVEL; level <= HIGH_LEVEL; level++)
	{
		delete recordLabel[level];
	}
}

void UserInfoDialog::setUser(User *user)
{
	//	设置要显示的用户

	this->user = user;
}

void UserInfoDialog::displayDialog()
{
	//	展示用户的数据信息

	static char text[INFO_MAX_LEN];

	ui->nameLabel->setText(user->getName());

	for (int level = EASY_LEVEL; level <= HIGH_LEVEL; level++)
	{
		snprintf(text, INFO_MAX_LEN, "%ds", user->getRecord(level));
		recordLabel[level]->setText((user->getRecord(level) != INT_MAX) ? text : "~");
	}
	this->exec();
}
