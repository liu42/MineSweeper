#ifndef __USER_H__
#define __USER_H__

#include <QTextStream>
#include <QVector>
#include <QString>
#include <QFile>
#include <QDir>

#include "config.h"

//	游戏中的用户类，保存用户的数据和相关操作

class User
{
	//	用户的数据信息，其中 index 表示用户在用户列表中的位置

	private:
		QString name;
		QString passwd;
		int record[LEVEL_COUNT];
		int index;

	//	静态成员方法用于读取和写入用户数据，获取用户列表

	public:
		static QVector <User> getUserList();
		static void checkDataFile();
		static void readDataFile();
		static void saveDataFile();

	public:
		QString getName();
		QString getPasswd();
		int getRecord(int);
		void setName(QString);
		void setPasswd(QString);

	public:
		bool login();
		bool registry();
		void addRecord(int, int);
};

#endif
