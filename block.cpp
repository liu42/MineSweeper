#include "block.h"

//	以下为相关属性的获取操作函数

int Block::getNumber()
{
	return number;
}

bool Block::getIsNull()
{
	return (type == NULL) ? true : false;
}

bool Block::getIsMine()
{
	return (type == MINE) ? true : false;
}

bool Block::getIsNumber()
{
	return (type == NUMBER) ? true : false;
}

bool Block::getIsCovered()
{
	return isCovered;
}

bool Block::getIsMarked()
{
	return isMarked;
}

bool Block::getIsError()
{
	return isError;
}

bool Block::getIsTouched()
{
	return isTouched;
}

//	以下为该方块在游戏中的相关操作函数

void Block::initData()
{
	//	初始化各项数据

	type = NULL;
	number = 0;
	isCovered = true;
	isMarked = false;
	isTouched = false;
	isError = false;
}

void Block::addMine()
{
	//	将该位置设为地雷

	type = MINE;
}

void Block::addNumber()
{
	//	将该位置设为数字同时将数字加一

	type = NUMBER;
	number += 1;
}

void Block::loseOperation()
{
	//	在游戏结束且失败时进行的操作

	if (type == MINE && !isMarked)
	{
		//  如果这个位置是地雷但没有被标记，则将这个位置打开

		isCovered = false;
	}
	else if (type != MINE && isMarked)
	{
		//  如果这个位置不是地雷却被标记了，则将这个位置标记为错误

		isError = true;
	}
}

void Block::leftClick()
{
	//	鼠标左键点击该位置的操作

	if (!isMarked)
	{
		//	将这个位置打开，如果这个位置是地雷则设为触碰到地雷

		isCovered = false;

		if (type == MINE)
		{
			isTouched = true;
		}
	}
}

void Block::rightClick(int &flagCount)
{
	//	鼠标右键键点击该位置的操作

	if (!isMarked)
	{
		//	如果这个位置没有被标记则将标记此位置

		isMarked = true;
		flagCount -= 1;
	}
	else
	{
		//	反之则取消标记，同时改变旗子的数量

		isMarked = false;
		flagCount += 1;
	}
}
